// UP :: FEUP :: M.EEC :: Software Design

#include "../include/gui/helloworld.h"
//#include "helloworld.h"
#include "gtkmm/application.h"

int main (int argc, char *argv[])
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  gui::HelloWorld helloworld;

  //Shows the window and returns when it is closed.
  return app->run(helloworld);
}
