# GTK_Glade

The hello world program in  Glade + GTK.
Written for cross platform VSCode.


Based on https://www.gtk.org/docs/getting-started/hello-world 


# Contributors: 
 * Armando Sousa -  [email](mailto:asousa@fe.up.pt)
 * Ricardo B. Sousa -  [email](mailto:rbs@fe.up.pt)

# License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
In short, free software: Useful, transparent, no warranty


# Details
Configuration of the GTK + Glade developer environment for Linux and Windows.
This configuration generates a window with only one button `[Hello World]`. If
you click in this button, the `std::cout` prints the string `"Hello World"`.

Even though Glade was not used to generate a XML file (`.glade`) for building
the GTK interface, Glade is also configured in this repository (not only the
installation but also the VSCode IntelliSense).

# Simple Build and Run

To build, press CTRL+SHIFT+B to run the makefile within VScode or run `make` in the terminal.

To run the executable Main.exe, press CTRL+F5 to run or run ./Main.exe in the terminal.
A windows appears; press button and see the terminal.


## Installation

### GTK

**Linux**

1. Install [GTK](https://www.gtk.org/) and
   [gtkmm](http://transit.iut2.upmf-grenoble.fr/doc/gtkmm-3.0/tutorial/html/index.html)

   - Install packages:

   ```sh
   # Install the GTK library package
   sudo apt install libgtk-3-0
   # Install the gtkmm C++ wrapper for GTK
   sudo apt install libgtkmm-3.0-dev libgtkmm-3.0-doc
   ```

   - Version 3.0 was installed given its compatability with Ubuntu 18 LTS
   - Install version 4.0 for using with Ubuntu 20 LTS

2. Install pkg-config (helper tool for compiling applications and libraries)

   ```sh
   sudo apt install pkg-config
   ```

**Windows**

The installation of `GTK` assumes that you have already installed `MSYS2` in
your PC.

1. Install [`GTK`](https://www.gtk.org/) and
   [`gtkmm`](http://transit.iut2.upmf-grenoble.fr/doc/gtkmm-3.0/tutorial/html/index.html)

   1. Open `MSYS2` terminal (Start menu > Search `MSYS2`)
   2. Execute the following commands:

   ```sh
   # Install the GTK library package
   pacman -S mingw-w64-x86_64-gtk3
   # Install the gtkmm C++ wrapper for GTK
   pacman -S mingw-w64-x86_64-gtkmm3
   ```

2. Install `pkgconf` (helper tool for compiling applications and libraries)

   ```sh
   pacman -S pkgconf
   ```

   - The `pkg-config` was not installed due to not being able to generate the
     proper include paths for compiling GTK applications in Windows

### Glade

**Linux**

```sh
sudo apt install glade
```

**Windows**

Open a `MSYS2` terminal and execute the following command:

```sh
pacman -S mingw-w64-x86_64-glade
```

### Visual Studio Code

Given the use of package configuration (`pkg-config` or `pkgconf` for Linux and
Windows, respectively) for building the application (the include paths are not
added to environement variables), it is required to add the `gtkmm` include
paths to VSCode's `includePath` setting.

**Linux**

1. Open a terminal
2. Execute the following commands:

   ```sh
   # Get dependencies of gtkmm
   # (in C++, we will use directly the C++ wrapper for GTK)
   $ pkg-config gtkmm-3.0 --cflags --libs
   # ...output...
   -pthread -I/usr/include/gtkmm-3.0 -I/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include -I/usr/include/atkmm-1.6 -I/usr/include/gtk-3.0/unix-print -I/usr/include/gdkmm-3.0 -I/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include -I/usr/include/giomm-2.4 -I/usr/lib/x86_64-linux-gnu/giomm-2.4/include -I/usr/include/pangomm-1.4 -I/usr/lib/x86_64-linux-gnu/pangomm-1.4/include -I/usr/include/glibmm-2.4 -I/usr/lib/x86_64-linux-gnu/glibmm-2.4/include -I/usr/include/gtk-3.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I/usr/include/gtk-3.0 -I/usr/include/gio-unix-2.0/ -I/usr/include/cairo -I/usr/include/pango-1.0 -I/usr/include/harfbuzz -I/usr/include/pango-1.0 -I/usr/include/atk-1.0 -I/usr/include/cairo -I/usr/include/cairomm-1.0 -I/usr/lib/x86_64-linux-gnu/cairomm-1.0/include -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/sigc++-2.0 -I/usr/lib/x86_64-linux-gnu/sigc++-2.0/include -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libpng16 -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -lgtkmm-3.0 -latkmm-1.6 -lgdkmm-3.0 -lgiomm-2.4 -lpangomm-1.4 -lglibmm-2.4 -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo-gobject -lgio-2.0 -lcairomm-1.0 -lcairo -lsigc-2.0 -lgdk_pixbuf-2.0 -lgobject-2.0 -lglib-2.0
   ```

3. Get the include paths from the `pkg-config`'s output:

   1. Double click in the VSCode file's bar to open a new file (you do not need
      to save it)
   2. Past the `pkg-config`'s output
   3. `Ctrl+f` to find all space characters (` `) in the file
   4. Create a line break in the end of the file, and select and copy it
   5. Past line break in the Replace (`Ctrl+f`) and replace all space characters
      by line breaks
   6. Eliminate the lines that do not contain `-I`

      - If you want, in this step, go to the VSCode definitions and copy the
        directories to the `includePath` setting of the C++ IntelliSense
      - Replace `-I` for an empty string to get the raw directories

   7. `Ctrl+f` to find `-I` and replace all for `"`
   8. Copy a line break, paste it in `Ctrl+f`, and replace all for `",` plus the
      line break (i.e., in replace, put the character `"` and paste the line
      break)

4. Add the paths to the project's `c_cpp_properties.json` file

- Expected directories:

  ```txt
  /usr/include/gtkmm-3.0
  /usr/lib/x86_64-linux-gnu/gtkmm-3.0/include
  /usr/include/atkmm-1.6
  /usr/include/gtk-3.0/unix-print
  /usr/include/gdkmm-3.0
  /usr/lib/x86_64-linux-gnu/gdkmm-3.0/include
  /usr/include/giomm-2.4
  /usr/lib/x86_64-linux-gnu/giomm-2.4/include
  /usr/include/pangomm-1.4
  /usr/lib/x86_64-linux-gnu/pangomm-1.4/include
  /usr/include/glibmm-2.4
  /usr/lib/x86_64-linux-gnu/glibmm-2.4/include
  /usr/include/gtk-3.0
  /usr/include/at-spi2-atk/2.0
  /usr/include/at-spi-2.0
  /usr/include/dbus-1.0
  /usr/lib/x86_64-linux-gnu/dbus-1.0/include
  /usr/include/gtk-3.0
  /usr/include/gio-unix-2.0/
  /usr/include/cairo
  /usr/include/pango-1.0
  /usr/include/harfbuzz
  /usr/include/pango-1.0
  /usr/include/atk-1.0
  /usr/include/cairo
  /usr/include/cairomm-1.0
  /usr/lib/x86_64-linux-gnu/cairomm-1.0/include
  /usr/include/cairo
  /usr/include/pixman-1
  /usr/include/freetype2
  /usr/include/libpng16
  /usr/include/freetype2
  /usr/include/libpng16
  /usr/include/sigc++-2.0
  /usr/lib/x86_64-linux-gnu/sigc++-2.0/include
  /usr/include/gdk-pixbuf-2.0
  /usr/include/libpng16
  /usr/include/glib-2.0
  /usr/lib/x86_64-linux-gnu/glib-2.0/include
  ```

- Expected `c_cpp_properties.json` file:

  ```json
  {
    "configurations": [
      {
        ...
        "includePath": [
          "${workspaceFolder}/include",
          "/usr/include/gtkmm-3.0",
          "/usr/lib/x86_64-linux-gnu/gtkmm-3.0/include",
          "/usr/include/atkmm-1.6",
          "/usr/include/gtk-3.0/unix-print",
          "/usr/include/gdkmm-3.0",
          "/usr/lib/x86_64-linux-gnu/gdkmm-3.0/include",
          "/usr/include/giomm-2.4",
          "/usr/lib/x86_64-linux-gnu/giomm-2.4/include",
          "/usr/include/pangomm-1.4",
          "/usr/lib/x86_64-linux-gnu/pangomm-1.4/include",
          "/usr/include/glibmm-2.4",
          "/usr/lib/x86_64-linux-gnu/glibmm-2.4/include",
          "/usr/include/gtk-3.0",
          "/usr/include/at-spi2-atk/2.0",
          "/usr/include/at-spi-2.0",
          "/usr/include/dbus-1.0",
          "/usr/lib/x86_64-linux-gnu/dbus-1.0/include",
          "/usr/include/gtk-3.0",
          "/usr/include/gio-unix-2.0/",
          "/usr/include/cairo",
          "/usr/include/pango-1.0",
          "/usr/include/harfbuzz",
          "/usr/include/pango-1.0",
          "/usr/include/atk-1.0",
          "/usr/include/cairo",
          "/usr/include/cairomm-1.0",
          "/usr/lib/x86_64-linux-gnu/cairomm-1.0/include",
          "/usr/include/cairo",
          "/usr/include/pixman-1",
          "/usr/include/freetype2",
          "/usr/include/libpng16",
          "/usr/include/freetype2",
          "/usr/include/libpng16",
          "/usr/include/sigc++-2.0",
          "/usr/lib/x86_64-linux-gnu/sigc++-2.0/include",
          "/usr/include/gdk-pixbuf-2.0",
          "/usr/include/libpng16",
          "/usr/include/glib-2.0",
          "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
        ],
        ...
  ```

**Windows**

1. Open a **bash** terminal (e.g., VSCode > Terminal > New terminal > `+` >
   bash) in Windows
2. Execute the following commands:

   ```sh
   # Get dependencies of gtkmm
   # (in C++, we will use directly the C++ wrapper for GTK)
   $ pkgconf gtkmm-3.0 --cflags --libs
   # ...output...
   -IC:/msys64/mingw64/bin/../include/gtkmm-3.0 -IC:/msys64/mingw64/bin/../lib/gtkmm-3.0/include -IC:/msys64/mingw64/bin/../include/giomm-2.4 -IC:/msys64/mingw64/bin/../lib/giomm-2.4/include -IC:/msys64/mingw64/bin/../include -IC:/msys64/mingw64/bin/../include/glib-2.0 -IC:/msys64/mingw64/bin/../lib/glib-2.0/include -mms-bitfields -IC:/msys64/mingw64/bin/../include/glibmm-2.4 -IC:/msys64/mingw64/bin/../lib/glibmm-2.4/include -IC:/msys64/mingw64/bin/../include/sigc++-2.0 -IC:/msys64/mingw64/bin/../lib/sigc++-2.0/include
   -IC:/msys64/mingw64/bin/../include/gtk-3.0 -IC:/msys64/mingw64/bin/../include/pango-1.0 -IC:/msys64/mingw64/bin/../include/harfbuzz -IC:/msys64/mingw64/bin/../include/freetype2 -IC:/msys64/mingw64/bin/../include/libpng16 -mms-bitfields -IC:/msys64/mingw64/bin/../include/fribidi -IC:/msys64/mingw64/bin/../include/cairo -IC:/msys64/mingw64/bin/../include/lzo -IC:/msys64/mingw64/bin/../include/pixman-1 -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -IC:/msys64/mingw64/bin/../include/gdk-pixbuf-2.0 -mms-bitfields -mms-bitfields -mms-bitfields -IC:/msys64/mingw64/bin/../include/atk-1.0 -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -IC:/msys64/mingw64/bin/../include/cairomm-1.0 -IC:/msys64/mingw64/bin/../lib/cairomm-1.0/include -IC:/msys64/mingw64/bin/../include/pangomm-1.4 -IC:/msys64/mingw64/bin/../lib/pangomm-1.4/include -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -IC:/msys64/mingw64/bin/../include/atkmm-1.6 -IC:/msys64/mingw64/bin/../lib/atkmm-1.6/include -IC:/msys64/mingw64/bin/../include/gdkmm-3.0 -IC:/msys64/mingw64/bin/../lib/gdkmm-3.0/include -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -mms-bitfields -pthread -mms-bitfields -LC:/msys64/mingw64/bin/../lib -lgtkmm-3.0 -latkmm-1.6 -lgdkmm-3.0 -lgiomm-2.4 -lgtk-3 -lgdk-3 -lz -lgdi32 -limm32 -lshell32 -lole32 -Wl,-luuid -lwinmm -ldwmapi
   -lsetupapi -lcfgmgr32 -lpangowin32-1.0 -latk-1.0 -lcairo-gobject -lgio-2.0 -lpangomm-1.4 -lglibmm-2.4 -lcairomm-1.0 -lsigc-2.0 -lpangocairo-1.0 -lpango-1.0 -lharfbuzz -lcairo -lgdk_pixbuf-2.0 -lgobject-2.0 -lglib-2.0 -lintl
   ```

   - **Note:** the line break added by `pkgconf` maybe coud be problematic in
     the future but, for now, it worked!

3. Get the include paths from the `pkgconf`'s output:

   1. Double click in the VSCode file's bar to open a new file (you do not need
      to save it)
   2. Past the `pkgconf`'s output
   3. `Ctrl+f` to find all space characters (` `) in the file
   4. Create a line break in the end of the file, and select and copy it
   5. Past line break in the Replace (`Ctrl+f`) and replace all space characters
      by line breaks
   6. Eliminate the lines that do not contain `-I`

      - If you want, in this step, go to the VSCode definitions and copy the
        directories to the `includePath` setting of the C++ IntelliSense

   7. `Ctrl+f` to find `-I` and replace all for `"`
   8. Copy a line break, paste it in `Ctrl+f`, and replace all for `",` plus the
      line break (i.e., in replace, put the character `"` and paste the line
      break)

4. Add the paths to the project's `c_cpp_properties.json` file

- Expected directories:

  ```txt
  C:/msys64/mingw64/bin/../include/gtkmm-3.0
  C:/msys64/mingw64/bin/../lib/gtkmm-3.0/include
  C:/msys64/mingw64/bin/../include/giomm-2.4
  C:/msys64/mingw64/bin/../lib/giomm-2.4/include
  C:/msys64/mingw64/bin/../include
  C:/msys64/mingw64/bin/../include/glib-2.0
  C:/msys64/mingw64/bin/../lib/glib-2.0/include
  C:/msys64/mingw64/bin/../include/glibmm-2.4
  C:/msys64/mingw64/bin/../lib/glibmm-2.4/include
  C:/msys64/mingw64/bin/../include/sigc++-2.0
  C:/msys64/mingw64/bin/../lib/sigc++-2.0/include
  C:/msys64/mingw64/bin/../include/gtk-3.0
  C:/msys64/mingw64/bin/../include/pango-1.0
  C:/msys64/mingw64/bin/../include/harfbuzz
  C:/msys64/mingw64/bin/../include/freetype2
  C:/msys64/mingw64/bin/../include/libpng16
  C:/msys64/mingw64/bin/../include/fribidi
  C:/msys64/mingw64/bin/../include/cairo
  C:/msys64/mingw64/bin/../include/lzo
  C:/msys64/mingw64/bin/../include/pixman-1
  C:/msys64/mingw64/bin/../include/gdk-pixbuf-2.0
  C:/msys64/mingw64/bin/../include/atk-1.0
  C:/msys64/mingw64/bin/../include/cairomm-1.0
  C:/msys64/mingw64/bin/../lib/cairomm-1.0/include
  C:/msys64/mingw64/bin/../include/pangomm-1.4
  C:/msys64/mingw64/bin/../lib/pangomm-1.4/include
  C:/msys64/mingw64/bin/../include/atkmm-1.6
  C:/msys64/mingw64/bin/../lib/atkmm-1.6/include
  C:/msys64/mingw64/bin/../include/gdkmm-3.0
  C:/msys64/mingw64/bin/../lib/gdkmm-3.0/include
  ```

- Expected `c_cpp_properties.json` file:

  ```json
  {
    "configurations": [
      {
        ...
        "includePath": [
          "${workspaceFolder}/**",
          "C:/msys64/mingw64/bin/../include/gtkmm-3.0",
          "C:/msys64/mingw64/bin/../lib/gtkmm-3.0/include",
          "C:/msys64/mingw64/bin/../include/giomm-2.4",
          "C:/msys64/mingw64/bin/../lib/giomm-2.4/include",
          "C:/msys64/mingw64/bin/../include",
          "C:/msys64/mingw64/bin/../include/glib-2.0",
          "C:/msys64/mingw64/bin/../lib/glib-2.0/include",
          "C:/msys64/mingw64/bin/../include/glibmm-2.4",
          "C:/msys64/mingw64/bin/../lib/glibmm-2.4/include",
          "C:/msys64/mingw64/bin/../include/sigc++-2.0",
          "C:/msys64/mingw64/bin/../lib/sigc++-2.0/include",
          "C:/msys64/mingw64/bin/../include/gtk-3.0",
          "C:/msys64/mingw64/bin/../include/pango-1.0",
          "C:/msys64/mingw64/bin/../include/harfbuzz",
          "C:/msys64/mingw64/bin/../include/freetype2",
          "C:/msys64/mingw64/bin/../include/libpng16",
          "C:/msys64/mingw64/bin/../include/fribidi",
          "C:/msys64/mingw64/bin/../include/cairo",
          "C:/msys64/mingw64/bin/../include/lzo",
          "C:/msys64/mingw64/bin/../include/pixman-1",
          "C:/msys64/mingw64/bin/../include/gdk-pixbuf-2.0",
          "C:/msys64/mingw64/bin/../include/atk-1.0",
          "C:/msys64/mingw64/bin/../include/cairomm-1.0",
          "C:/msys64/mingw64/bin/../lib/cairomm-1.0/include",
          "C:/msys64/mingw64/bin/../include/pangomm-1.4",
          "C:/msys64/mingw64/bin/../lib/pangomm-1.4/include",
          "C:/msys64/mingw64/bin/../include/atkmm-1.6",
          "C:/msys64/mingw64/bin/../lib/atkmm-1.6/include",
          "C:/msys64/mingw64/bin/../include/gdkmm-3.0",
          "C:/msys64/mingw64/bin/../lib/gdkmm-3.0/include"
        ],
        ...
  ```

## Usage

### Linux

```sh
# Clean all the binaries and objects present in the workspace
make clean
# Build the executable file (optional - run already build the executable)
make
# Run
make run
```

### Windows

Execute the following commands in a bash terminal (e.g., VSCode > Terminal >
New terminal > `+` > bash) for openning a MinGW terminal:

```sh
# Clean all the binaries and objects present in the workspace
mingw32-make clean
# Build the executable file (optional - run already build the executable)
mingw32-make
# Run
mingw32-make run
```


## More Information and install commands

https://packages.msys2.org/package/mingw-w64-x86_64-gtkmm
pacman -S mingw-w64-x86_64-gtkmm

